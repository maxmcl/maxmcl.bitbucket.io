<!DOCTYPE html>
<html lang="en" prefix="og: http://ogp.me/ns#">
<head>
  <meta charset="UTF-8" />
  <title>gdb adventure: Grokking <cite>std::string</cite>'s memory layout | maxmcl.bitbucket.io</title>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,600i%7CSource+Code+Pro:400,400i,600" />
  <link rel="stylesheet" href="https://maxmcl.bitbucket.io/static/m-dark.compiled.css" />
  <link rel="icon" href="https://maxmcl.bitbucket.io/static/favicon.ico" type="image/x-ico" />
  <link rel="canonical" href="https://maxmcl.bitbucket.io/blog/grok-strings/" />
  <link href="https://maxmcl.bitbucket.io/feeds/all.atom.xml" type="application/atom+xml" rel="alternate" title="maxmcl.bitbucket.io" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="theme-color" content="#22272e" />
  <meta property="og:site_name" content="maxmcl.bitbucket.io" />
  <meta property="og:title" content="gdb adventure: Grokking <cite>std::string</cite>'s memory layout" />
  <meta name="twitter:title" content="gdb adventure: Grokking <cite>std::string</cite>'s memory layout" />
  <meta property="og:url" content="https://maxmcl.bitbucket.io/blog/grok-strings/" />
  <meta property="og:description" content="A std::string case study with gdb" />
  <meta name="twitter:description" content="A std::string case study with gdb" />
  <meta property="og:image" content="https://maxmcl.bitbucket.io/static/string.png" />
  <meta name="twitter:image" content="https://maxmcl.bitbucket.io/static/string.png" />
  <meta name="twitter:card" content="summary_large_image" />
  <meta property="og:type" content="article" />
</head>
<body>
<header><nav id="navigation" class="m-navbar-cover">
  <div class="m-container">
    <div class="m-row">
      <a href="https://maxmcl.bitbucket.io/" id="m-navbar-brand" class="m-col-t-9 m-col-m-none m-left-m"><img src="https://maxmcl.bitbucket.io/static/favicon.ico" alt="" />maxmcl.bitbucket.io</a>
      <a id="m-navbar-show" href="#navigation" title="Show navigation" class="m-col-t-3 m-hide-m m-text-right"></a>
      <a id="m-navbar-hide" href="#" title="Hide navigation" class="m-col-t-3 m-hide-m m-text-right"></a>
      <div id="m-navbar-collapse" class="m-col-t-12 m-show-m m-col-m-none m-right-m">
        <div class="m-row">
          <ol class="m-col-t-12 m-col-m-none">
            <li><a href="https://maxmcl.bitbucket.io/">Blog</a></li>
            <li><a href="https://maxmcl.bitbucket.io/contact/">Contact</a></li>
          </ol>
        </div>
      </div>
    </div>
  </div>
</nav></header>
<main>
  <article id="m-jumbo">
    <header>
      <div id="m-jumbo-image" style="background-image: url('https://maxmcl.bitbucket.io/static/string.png');">
        <div id="m-jumbo-cover">
          <div class="m-container">
            <div class="m-row">
              <div class="m-col-t-6 m-col-s-5 m-push-s-1 m-text-left">Sun 01 May 2022</div>
              <div class="m-col-t-6 m-col-s-5 m-push-s-1 m-text-right"><a href="https://maxmcl.bitbucket.io/author/maxmcl/">maxmcl</a></div>
            </div>
            <div class="m-row">
              <div class="m-col-t-12 m-col-s-10 m-push-s-1 m-col-m-8 m-push-m-2">
                <h1><a href="https://maxmcl.bitbucket.io/blog/grok-strings/" rel="bookmark" title="Permalink to gdb adventure: Grokking <cite>std::string</cite>'s memory layout">gdb adventure: Grokking <cite>std::string</cite>'s memory layout</a></h1>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
    <div class="m-container m-container-inflatable">
      <div class="m-row">
        <div class="m-col-m-10 m-push-m-1 m-nopady">
<!-- content -->
<p>Playing with <cite>gdb</cite> and grokking C++ <cite>std::string</cite>'s memory layout. All experiments
are compiled with <cite>gcc 9.4.0</cite>, which is the default version on Ubuntu 20.04.</p>
<aside class="contents" id="contents">
<h3>Contents</h3>
<ul>
<li><a href="#a-detour-on-strings" id="toc-entry-1">A detour on strings</a></li>
<li><a href="#first-inspection-in-gdb" id="toc-entry-2">First inspection in gdb</a></li>
<li><a href="#examining-std-string" id="toc-entry-3">Examining `std::string`</a></li>
</ul>
</aside>
<p></p>
<section id="a-detour-on-strings">
<h2><a href="#a-detour-on-strings">A detour on strings</a></h2>
<p>What is a string? Conceptually, strings are nothing more than a sequence of
<cite>uint8</cite> values. The C-string implementation famously <cite>null-terminates</cite> them, i.e.
a null byte <cite>0</cite> is appended at the end of the array (sequence) to indicate the
end of the string. This makes it so it's unnecessary to pass the length of the array
around, but can lead to overflow issues if manipulated.</p>
<p>However we can simply think of them as <cite>std::array&lt;char&gt;</cite> or <cite>std::vector&lt;char&gt;</cite>
depending if they are <cite>stack</cite> or <cite>heap</cite> allocated. Lets assume they will be heap
allocated. It seems logical to assume <cite>string</cite> s would behave like <cite>vector</cite> s,
that is they would allocate a contiguous chunk of memory on the heap and
maintain a pointer to it and its length.</p>
<p>This represents our mental model so far</p>
<pre class="m-code"><span class="k">struct</span><span class="w"> </span><span class="nc">String</span><span class="w"> </span><span class="p">{</span>
<span class="w">  </span><span class="kt">size_t</span><span class="w"> </span><span class="n">length</span><span class="p">;</span>
<span class="w">  </span><span class="kt">char</span><span class="w"> </span><span class="o">*</span><span class="n">buffer</span><span class="p">;</span>
<span class="p">}</span></pre>
<p>and this could be a potential very simplified and not production ready implementation:</p>
<pre class="m-code"><span class="cp">#include</span><span class="w"> </span><span class="cpf">&lt;cstring&gt;</span>
<span class="cp">#include</span><span class="w"> </span><span class="cpf">&lt;iostream&gt;</span>

<span class="k">struct</span><span class="w"> </span><span class="nc">String</span><span class="w"> </span><span class="p">{</span>
<span class="w">  </span><span class="kt">size_t</span><span class="w"> </span><span class="n">length</span><span class="p">;</span>
<span class="w">  </span><span class="kt">char</span><span class="w"> </span><span class="o">*</span><span class="n">buffer</span><span class="p">;</span>

<span class="w">  </span><span class="n">String</span><span class="p">(</span><span class="kt">size_t</span><span class="w"> </span><span class="n">_length</span><span class="p">,</span><span class="w"> </span><span class="k">const</span><span class="w"> </span><span class="kt">char</span><span class="w"> </span><span class="o">*</span><span class="n">value</span><span class="w"> </span><span class="o">=</span><span class="w"> </span><span class="k">nullptr</span><span class="p">)</span>
<span class="w">      </span><span class="o">:</span><span class="w"> </span><span class="n">length</span><span class="p">(</span><span class="n">_length</span><span class="p">),</span><span class="w"> </span><span class="n">buffer</span><span class="p">(</span><span class="k">new</span><span class="w"> </span><span class="kt">char</span><span class="p">[</span><span class="n">length</span><span class="p">])</span><span class="w"> </span><span class="p">{</span>
<span class="w">    </span><span class="k">if</span><span class="w"> </span><span class="p">(</span><span class="n">value</span><span class="p">)</span><span class="w"> </span><span class="p">{</span>
<span class="w">      </span><span class="c1">// The user is responsible to make sure value&#39;s size is &lt;= length</span>
<span class="w">      </span><span class="n">std</span><span class="o">::</span><span class="n">memcpy</span><span class="p">(</span><span class="n">buffer</span><span class="p">,</span><span class="w"> </span><span class="n">value</span><span class="p">,</span><span class="w"> </span><span class="n">length</span><span class="p">);</span>
<span class="w">    </span><span class="p">}</span>
<span class="w">  </span><span class="p">};</span>
<span class="w">  </span><span class="o">~</span><span class="n">String</span><span class="p">()</span><span class="w"> </span><span class="p">{</span><span class="w"> </span><span class="k">delete</span><span class="p">[]</span><span class="w"> </span><span class="n">buffer</span><span class="p">;</span><span class="w"> </span><span class="p">};</span>
<span class="w">  </span><span class="n">String</span><span class="p">(</span><span class="k">const</span><span class="w"> </span><span class="n">String</span><span class="w"> </span><span class="o">&amp;</span><span class="n">other</span><span class="p">)</span><span class="w"> </span><span class="o">:</span><span class="w"> </span><span class="n">length</span><span class="p">(</span><span class="n">other</span><span class="p">.</span><span class="n">length</span><span class="p">),</span><span class="w"> </span><span class="n">buffer</span><span class="p">(</span><span class="k">new</span><span class="w"> </span><span class="kt">char</span><span class="p">[</span><span class="n">length</span><span class="p">])</span><span class="w"> </span><span class="p">{</span>
<span class="w">    </span><span class="o">*</span><span class="n">buffer</span><span class="w"> </span><span class="o">=</span><span class="w"> </span><span class="o">*</span><span class="n">other</span><span class="p">.</span><span class="n">buffer</span><span class="p">;</span>
<span class="w">  </span><span class="p">};</span>
<span class="w">  </span><span class="n">String</span><span class="w"> </span><span class="k">operator</span><span class="o">=</span><span class="p">(</span><span class="k">const</span><span class="w"> </span><span class="n">String</span><span class="w"> </span><span class="o">&amp;</span><span class="n">other</span><span class="p">)</span><span class="w"> </span><span class="p">{</span>
<span class="w">    </span><span class="n">length</span><span class="w"> </span><span class="o">=</span><span class="w"> </span><span class="n">other</span><span class="p">.</span><span class="n">length</span><span class="p">;</span>
<span class="w">    </span><span class="o">*</span><span class="n">buffer</span><span class="w"> </span><span class="o">=</span><span class="w"> </span><span class="o">*</span><span class="n">other</span><span class="p">.</span><span class="n">buffer</span><span class="p">;</span>
<span class="w">    </span><span class="k">return</span><span class="w"> </span><span class="o">*</span><span class="k">this</span><span class="p">;</span>
<span class="w">  </span><span class="p">}</span>
<span class="w">  </span><span class="c1">// We take ownership of other, but we had nothing to free yet</span>
<span class="w">  </span><span class="n">String</span><span class="p">(</span><span class="n">String</span><span class="w"> </span><span class="o">&amp;&amp;</span><span class="n">other</span><span class="p">)</span><span class="w"> </span><span class="o">:</span><span class="w"> </span><span class="n">length</span><span class="p">(</span><span class="n">other</span><span class="p">.</span><span class="n">length</span><span class="p">),</span><span class="w"> </span><span class="n">buffer</span><span class="p">(</span><span class="k">nullptr</span><span class="p">)</span><span class="w"> </span><span class="p">{</span>
<span class="w">    </span><span class="n">std</span><span class="o">::</span><span class="n">swap</span><span class="p">(</span><span class="n">buffer</span><span class="p">,</span><span class="w"> </span><span class="n">other</span><span class="p">.</span><span class="n">buffer</span><span class="p">);</span>
<span class="w">  </span><span class="p">}</span>
<span class="w">  </span><span class="c1">// We take ownership of other, but we want it to free our memory</span>
<span class="w">  </span><span class="n">String</span><span class="w"> </span><span class="k">operator</span><span class="o">=</span><span class="p">(</span><span class="n">String</span><span class="w"> </span><span class="o">&amp;&amp;</span><span class="n">other</span><span class="p">)</span><span class="w"> </span><span class="p">{</span>
<span class="w">    </span><span class="n">length</span><span class="w"> </span><span class="o">=</span><span class="w"> </span><span class="n">other</span><span class="p">.</span><span class="n">length</span><span class="p">;</span>
<span class="w">    </span><span class="n">std</span><span class="o">::</span><span class="n">swap</span><span class="p">(</span><span class="n">buffer</span><span class="p">,</span><span class="w"> </span><span class="n">other</span><span class="p">.</span><span class="n">buffer</span><span class="p">);</span>
<span class="w">    </span><span class="k">return</span><span class="w"> </span><span class="o">*</span><span class="k">this</span><span class="p">;</span>
<span class="w">  </span><span class="p">}</span>

<span class="w">  </span><span class="k">friend</span><span class="w"> </span><span class="n">std</span><span class="o">::</span><span class="n">ostream</span><span class="w"> </span><span class="o">&amp;</span><span class="k">operator</span><span class="o">&lt;&lt;</span><span class="p">(</span><span class="n">std</span><span class="o">::</span><span class="n">ostream</span><span class="w"> </span><span class="o">&amp;</span><span class="n">o</span><span class="p">,</span><span class="w"> </span><span class="k">const</span><span class="w"> </span><span class="n">String</span><span class="w"> </span><span class="o">&amp;</span><span class="n">value</span><span class="p">)</span><span class="w"> </span><span class="p">{</span>
<span class="w">    </span><span class="n">o</span><span class="w"> </span><span class="o">&lt;&lt;</span><span class="w"> </span><span class="s">&quot;String: &quot;</span><span class="p">;</span>
<span class="w">    </span><span class="n">o</span><span class="p">.</span><span class="n">write</span><span class="p">(</span><span class="n">value</span><span class="p">.</span><span class="n">buffer</span><span class="p">,</span><span class="w"> </span><span class="n">value</span><span class="p">.</span><span class="n">length</span><span class="p">);</span>
<span class="w">    </span><span class="k">return</span><span class="w"> </span><span class="n">o</span><span class="p">;</span>
<span class="w">  </span><span class="p">}</span>

<span class="w">  </span><span class="kt">void</span><span class="w"> </span><span class="n">emplace</span><span class="p">(</span><span class="k">const</span><span class="w"> </span><span class="kt">char</span><span class="w"> </span><span class="o">*</span><span class="n">value</span><span class="p">)</span><span class="w"> </span><span class="p">{</span><span class="w"> </span><span class="n">std</span><span class="o">::</span><span class="n">memcpy</span><span class="p">(</span><span class="n">buffer</span><span class="p">,</span><span class="w"> </span><span class="n">value</span><span class="p">,</span><span class="w"> </span><span class="n">length</span><span class="p">);</span><span class="w"> </span><span class="p">}</span>
<span class="p">};</span></pre>
<p>Designing a remotely good string implementation was not the point of this experiment,
so lets just confirm this works by adding a simple program that tests all the
functionalities:</p>
<pre class="m-code"><span class="kt">int</span><span class="w"> </span><span class="nf">main</span><span class="p">()</span><span class="w"> </span><span class="p">{</span>
<span class="w">  </span><span class="n">String</span><span class="w"> </span><span class="n">s</span><span class="p">(</span><span class="mi">5</span><span class="p">,</span><span class="w"> </span><span class="s">&quot;abcde&quot;</span><span class="p">);</span><span class="w"> </span><span class="c1">// length == 5</span>
<span class="w">  </span><span class="n">String</span><span class="w"> </span><span class="n">ss</span><span class="p">(</span><span class="mi">10</span><span class="p">);</span>
<span class="w">  </span><span class="n">ss</span><span class="p">.</span><span class="n">emplace</span><span class="p">(</span><span class="s">&quot;Very long sentence&quot;</span><span class="p">);</span><span class="w"> </span><span class="c1">// length &gt;= 10</span>
<span class="w">  </span><span class="n">String</span><span class="w"> </span><span class="n">sss</span><span class="w"> </span><span class="o">=</span><span class="w"> </span><span class="n">std</span><span class="o">::</span><span class="n">move</span><span class="p">(</span><span class="n">ss</span><span class="p">);</span>
<span class="w">  </span><span class="n">std</span><span class="o">::</span><span class="n">cout</span><span class="w"> </span><span class="o">&lt;&lt;</span><span class="w"> </span><span class="n">s</span><span class="w"> </span><span class="o">&lt;&lt;</span><span class="w"> </span><span class="sc">&#39;\n&#39;</span><span class="p">;</span>
<span class="w">  </span><span class="n">s</span><span class="w"> </span><span class="o">=</span><span class="w"> </span><span class="n">std</span><span class="o">::</span><span class="n">move</span><span class="p">(</span><span class="n">sss</span><span class="p">);</span>
<span class="w">  </span><span class="n">std</span><span class="o">::</span><span class="n">cout</span><span class="w"> </span><span class="o">&lt;&lt;</span><span class="w"> </span><span class="n">s</span><span class="w"> </span><span class="o">&lt;&lt;</span><span class="w"> </span><span class="n">std</span><span class="o">::</span><span class="n">endl</span><span class="p">;</span>
<span class="p">}</span></pre>
<p>We can compile it with sanitizers on to make sure we are not doing anything wrong
with our memory allocations and manipulations:</p>
<pre class="m-code">g++<span class="w"> </span>-fsanitize<span class="o">=</span>address,undefined<span class="w"> </span>string.cpp</pre>
<p>This outputs</p>
<pre>String: abcde
String: Very long</pre>
<p>which is what we expected.</p>
<p>Finally, lets print the size in bytes that one of our strings occupies in memory.
We can change the last line of our <cite>main</cite> to:</p>
<pre class="m-code"><span class="n">std</span><span class="o">::</span><span class="n">cout</span><span class="w"> </span><span class="o">&lt;&lt;</span><span class="w"> </span><span class="n">s</span><span class="w"> </span><span class="o">&lt;&lt;</span><span class="w"> </span><span class="sc">&#39;\n&#39;</span><span class="w"> </span><span class="o">&lt;&lt;</span><span class="w"> </span><span class="k">sizeof</span><span class="p">(</span><span class="n">s</span><span class="p">)</span><span class="w"> </span><span class="o">&lt;&lt;</span><span class="w"> </span><span class="n">std</span><span class="o">::</span><span class="n">endl</span><span class="p">;</span></pre>
<p>and we get the new output:</p>
<pre>16</pre>
<p>which makes sense since both a pointer and <cite>size_t</cite> occupy 8 bytes each.</p>
</section>
<section id="first-inspection-in-gdb">
<h2><a href="#first-inspection-in-gdb">First inspection in gdb</a></h2>
<p>Let's recompile our program with debug symbols and look at the memory layout of
one of our objects in <cite>gdb</cite> . For this, I will also remove the sanitizers and
put the lowest level of compiler optimizations.</p>
<pre class="m-code">g++<span class="w"> </span>-g<span class="w"> </span>-O0<span class="w"> </span>string.cpp</pre>
<p>We can break on line 49 (before the <cite>move</cite> call) such that <cite>s</cite> and <cite>ss</cite> contain
values and print them both:</p>
<pre class="m-code">(gdb) p s
$1 = {length = 5, buffer = 0x55555556aeb0 &quot;abcde&quot;}
(gdb) p ss
$2 = {length = 10, buffer = 0x55555556aed0 &quot;Very long &quot;}</pre>
<p><cite>gdb</cite> is actually pretty helpful here and prints all the member properties, as
well as which character sequence their <cite>buffer</cite> s are pointing to.</p>
<p>Lets take it a step further and print their addresses in the memory:</p>
<pre class="m-code">(gdb) p &amp;s
$3 = (String *) 0x7fffffffdc80
(gdb) p &amp;ss
$4 = (String *) 0x7fffffffdc90</pre>
<p>right away we can recognize the large gap in the address space between the <cite>buffer</cite>
pointer values and the <cite>String</cite> objects. As it turns out, that is because the
pointer point to <cite>heap</cite> addresses, whereas the objects are allocated on the <cite>stack</cite>:</p>
<pre class="m-code"> (gdb) info proc mappings
 process 187087
 Mapped address spaces:

           Start Addr           End Addr       Size     Offset objfile
       ...
       0x555555559000     0x55555557a000    0x21000        0x0 [heap]
       ...
       0x7ffffffde000     0x7ffffffff000    0x21000        0x0 [stack]
       ...
   0xffffffffff600000 0xffffffffff601000     0x1000        0x0 [vsyscall]

(a lot of lines omitted). To wrap up our inspection, lets examinate the stack
on the 2 addresses of the `String` objects, recalling they are both 16 bytes
long:

.. code:: gdb

 (gdb) x/16bx 0x7fffffffdc80
 0x7fffffffdc80:       0x05    0x00    0x00    0x00    0x00    0x00    0x00    0x00
 0x7fffffffdc88:       0xb0    0xae    0x56    0x55    0x55    0x55    0x00    0x00

 (gdb) x/16bx 0x7fffffffdc90
 0x7fffffffdc90:       0x0a    0x00    0x00    0x00    0x00    0x00    0x00    0x00
 0x7fffffffdc98:       0xd0    0xae    0x56    0x55    0x55    0x55    0x00    0x00</pre>
<p>we can quickly spot that the first line contains the length (<cite>0x0a</cite> is 10) and
the second the heap address we've seen before. Using <cite>2gx</cite> makes it easier to see:</p>
<pre class="m-code">(gdb) x/2gx 0x7fffffffdc80
0x7fffffffdc80:       0x0000000000000005      0x000055555556aeb0

(gdb) x/2gx 0x7fffffffdc90
0x7fffffffdc90:       0x000000000000000a      0x000055555556aed0</pre>
<p><cite>g</cite> prints 16 bytes at once instead of a single one for <cite>b</cite>. Finally, we can
make sure our content is properly stored on the heap by reading the <cite>length</cite> from
the respective buffer addresses... or letting <cite>gdb</cite> treat them as <cite>C-string</cite> s
since they all null-terminated anyways:</p>
<pre class="m-code">(gdb) x/s 0x000055555556aeb0
0x55555556aeb0:       &quot;abcde&quot;

(gdb) x/s 0x000055555556aed0
0x55555556aed0:       &quot;Very long &quot;</pre>
<p>Everything is as expected, lets now see if this can translate to <cite>std::string</cite></p>
</section>
<section id="examining-std-string">
<h2><a href="#examining-std-string">Examining `std::string`</a></h2>
<p>We can now safely trash away all the previous code, we won't need it. We could
obviously just look at the C++ standard lib source code, but where's the fun in
that? Maybe we'll save it for the end.</p>
<p>Lets begin by making a simple program that prints the size of various strings:</p>
<pre class="m-code"><span class="cp">#include</span><span class="w"> </span><span class="cpf">&lt;iostream&gt;</span>
<span class="cp">#include</span><span class="w"> </span><span class="cpf">&lt;string&gt;</span>

<span class="kt">int</span><span class="w"> </span><span class="nf">main</span><span class="p">()</span><span class="w"> </span><span class="p">{</span>
<span class="w">  </span><span class="k">auto</span><span class="w"> </span><span class="n">x</span><span class="w"> </span><span class="o">=</span><span class="w"> </span><span class="n">std</span><span class="o">::</span><span class="n">string</span><span class="p">{</span><span class="s">&quot;ABCD&quot;</span><span class="p">};</span>
<span class="w">  </span><span class="k">auto</span><span class="w"> </span><span class="n">y</span><span class="w"> </span><span class="o">=</span><span class="w"> </span><span class="n">std</span><span class="o">::</span><span class="n">string</span><span class="p">(</span><span class="mi">48</span><span class="p">,</span><span class="w"> </span><span class="sc">&#39;A&#39;</span><span class="p">);</span>
<span class="w">  </span><span class="n">std</span><span class="o">::</span><span class="n">cout</span><span class="w"> </span><span class="o">&lt;&lt;</span><span class="w"> </span><span class="k">sizeof</span><span class="p">(</span><span class="n">x</span><span class="p">)</span><span class="w"> </span><span class="o">&lt;&lt;</span><span class="w"> </span><span class="s">&quot; &quot;</span><span class="w"> </span><span class="o">&lt;&lt;</span><span class="w"> </span><span class="k">sizeof</span><span class="p">(</span><span class="n">std</span><span class="o">::</span><span class="n">string</span><span class="p">{})</span><span class="w"> </span><span class="o">&lt;&lt;</span><span class="w"> </span><span class="s">&quot; &quot;</span><span class="w"> </span><span class="o">&lt;&lt;</span><span class="w"> </span><span class="k">sizeof</span><span class="p">(</span><span class="n">y</span><span class="p">)</span>
<span class="w">            </span><span class="o">&lt;&lt;</span><span class="w"> </span><span class="n">std</span><span class="o">::</span><span class="n">endl</span><span class="p">;</span>
<span class="p">}</span></pre>
<p>We can again compile it in debug mode and without optimizations to get the
following output:</p>
<pre>32 32 32</pre>
<p>Uh. So all 3 strings were allocated <cite>more</cite> memory than our implementation, but
their size also didn't vary with the length of the character sequence.</p>
<p>Lets move to <cite>gdb</cite> and inspect <cite>x</cite> and <cite>y</cite> like we did before:</p>
<pre class="m-code">(gdb) p x
$1 = &quot;ABCD&quot;

(gdb) p y
$2 = &#39;A&#39; &lt;repeats 48 times&gt;

(gdb) p &amp;x
$3 = (std::__cxx11::basic_string&lt;char, std::char_traits&lt;char&gt;, std::allocator&lt;char&gt; &gt; *) 0x7fffffffdc80

(gdb) p &amp;y
$4 = (std::__cxx11::basic_string&lt;char, std::char_traits&lt;char&gt;, std::allocator&lt;char&gt; &gt; *) 0x7fffffffdca0</pre>
<p>Now instead of seeing the helpful output we had, we can only see the string's value.
I presume this has to do with the <cite>libstdc++</cite> our program is linking against
not being a debug build.</p>
<pre class="m-code">ldd<span class="w"> </span>a.out
<span class="w">    </span>linux-vdso.so.1<span class="w"> </span><span class="o">(</span>0x00007ffdb09e2000<span class="o">)</span>
<span class="w">    </span>libstdc++.so.6<span class="w"> </span><span class="o">=</span>&gt;<span class="w"> </span>/lib/x86_64-linux-gnu/libstdc++.so.6<span class="w"> </span><span class="o">(</span>0x00007fa8d06b5000<span class="o">)</span>
<span class="w">    </span>libgcc_s.so.1<span class="w"> </span><span class="o">=</span>&gt;<span class="w"> </span>/lib/x86_64-linux-gnu/libgcc_s.so.1<span class="w"> </span><span class="o">(</span>0x00007fa8d069a000<span class="o">)</span>
<span class="w">    </span>libc.so.6<span class="w"> </span><span class="o">=</span>&gt;<span class="w"> </span>/lib/x86_64-linux-gnu/libc.so.6<span class="w"> </span><span class="o">(</span>0x00007fa8d04a8000<span class="o">)</span>
<span class="w">    </span>libm.so.6<span class="w"> </span><span class="o">=</span>&gt;<span class="w"> </span>/lib/x86_64-linux-gnu/libm.so.6<span class="w"> </span><span class="o">(</span>0x00007fa8d0359000<span class="o">)</span>
<span class="w">    </span>/lib64/ld-linux-x86-64.so.2<span class="w"> </span><span class="o">(</span>0x00007fa8d08b2000<span class="o">)</span>

file<span class="w"> </span>/lib/x86_64-linux-gnu/libstdc++.so.6
/lib/x86_64-linux-gnu/libstdc++.so.6:<span class="w"> </span>symbolic<span class="w"> </span>link<span class="w"> </span>to<span class="w"> </span>libstdc++.so.6.0.28

file<span class="w"> </span>/lib/x86_64-linux-gnu/libstdc++.so.6.0.28
/lib/x86_64-linux-gnu/libstdc++.so.6.0.28:<span class="w"> </span>ELF<span class="w"> </span><span class="m">64</span>-bit<span class="w"> </span>LSB<span class="w"> </span>shared<span class="w"> </span>object,<span class="w"> </span>x86-64,<span class="w"> </span>version<span class="w"> </span><span class="m">1</span><span class="w"> </span><span class="o">(</span>GNU/Linux<span class="o">)</span>,<span class="w"> </span>dynamically<span class="w"> </span>linked,<span class="w"> </span>BuildID<span class="o">[</span>sha1<span class="o">]=</span>c90e6603c7cdf84713cd445700a575d3ea446d9b,<span class="w"> </span>stripped</pre>
<p>after following the symbolic link to the actual shared lib we can see that it is
in fact stripped. Oh well. So back to our addresses, we can examine the stack
as we did before, this time reading 32 bytes or <cite>4g</cite> s:</p>
<pre class="m-code">(gdb) x/32bx 0x7fffffffdc80
0x7fffffffdc80:       0x90    0xdc    0xff    0xff    0xff    0x7f    0x00    0x00
0x7fffffffdc88:       0x04    0x00    0x00    0x00    0x00    0x00    0x00    0x00
0x7fffffffdc90:       0x41    0x42    0x43    0x44    0x00    0x7f    0x00    0x00
0x7fffffffdc98:       0x82    0x54    0x55    0x55    0x55    0x55    0x00    0x00

(gdb) x/32bx 0x7fffffffdca0
0x7fffffffdca0:       0xb0    0xae    0x56    0x55    0x55    0x55    0x00    0x00
0x7fffffffdca8:       0x30    0x00    0x00    0x00    0x00    0x00    0x00    0x00
0x7fffffffdcb0:       0x30    0x00    0x00    0x00    0x00    0x00    0x00    0x00
0x7fffffffdcb8:       0x90    0x54    0x55    0x55    0x55    0x55    0x00    0x00</pre>
<p>Something interesting happens. At offset <cite>0x7fffffffdc90</cite>, we can see something
familiar: <cite>0x41 0x42 0x43 0x44</cite>, which is one of our input sequences, <cite>ABCD</cite>.</p>
<p>Printing <cite>&amp;x</cite> as <cite>4gx</cite></p>
<pre class="m-code">(gdb) x/4gx 0x7fffffffdc80
0x7fffffffdc80:       0x00007fffffffdc90      0x0000000000000004
0x7fffffffdc90:       0x00007f0044434241      0x0000555555555482</pre>
<p>we can spot another interesting thing. The first 8 bytes contain the address
where we spotted <cite>ABCD</cite>, which is allocated in the 32 bytes reserved for the string
and the second appears to contain the length.</p>
<p>So the <cite>stdlib</cite>'s implementation is quite similar to ours as it turns out, but
they appear to have some kind of extra stack-allocated buffer where they store
the data. Odd. The rest looks like nothing recognizable, so it's probably unused
memory.</p>
<p>Lets now go back to <cite>&amp;y</cite> and do the same thing:</p>
<pre class="m-code">(gdb) x/4gx 0x7fffffffdca0
0x7fffffffdca0:       0x000055555556aeb0      0x0000000000000030
0x7fffffffdcb0:       0x0000000000000030      0x0000555555555490</pre>
<p>this time, we can spot right away that the address is heap allocated, and 48
<cite>A</cite> s are nowhere to be found in the stack-allocated buffer. We can also see
that the length of 48 <cite>0x30</cite> was written in the stack allocated buffer as well
as in the second 8 bytes chunk. Not sure why!</p>
<p>For everything to make sense, we should be able to retrieve our character
sequences by reading from the internal pointers addresses like we did for our
implementation:</p>
<pre class="m-code">(gdb) x/s 0x00007fffffffdc90
0x7fffffffdc90:       &quot;ABCD&quot;

(gdb) x/s 0x000055555556aeb0
0x55555556aeb0:       &#39;A&#39; &lt;repeats 48 times&gt;</pre>
<p>in the case of <cite>&amp;x</cite>, it simply confirms what we were able to observe directly
and, in the case of <cite>&amp;y</cite>, it confirms what we assumed.</p>
<p>It turns out this discrepancy is because of <cite>SSO</cite>, or <cite>small strings optimization</cite>
(and not <cite>single sign on</cite>). [IBM](<a class="m-link-wrap" href="https://www.ibm.com/support/pages/small-string-optimized-sso-string-class">https://www.ibm.com/support/pages/small-string-optimized-sso-string-class</a>) have a small (although not directly related)
explanation for a more generic memory optimization technique called <cite>small buffer
optimization</cite>, which is essentially the same but for generic buffers:</p>
<pre>The Small Buffer Optimization (SBO) is a well-known optimization technique that can reduce the number of dynamic memory allocations that occur at program execution time. Stack memory allocations are faster than heap memory allocations. The idea behind SBO is that if an object that would normally be created on the heap is small enough to fit inside of a small buffer allocated for the object, the application will use the buffer for storage instead of requesting memory to be allocated dynamically from the heap. This reduces the runtime overhead typically associated with dynamic memory allocation and can thereby improve runtime performance.</pre>
<p>In a nutshell, allocating on the stack is much faster and most applications tend
to frequently allocate small objects. This makes the overhead even larger so it's
preferable to reserve stack memory to allocate them there.</p>
</section>
<!-- /content -->
        </div>
      </div>
    </div>
    <footer class="m-container">
      <div class="m-row">
        <div class="m-col-m-10 m-push-m-1 m-nopadb">
          <p>Tags: <a href="https://maxmcl.bitbucket.io/tag/c/">C++</a>, <a href="https://maxmcl.bitbucket.io/tag/low-level/">low level</a>, <a href="https://maxmcl.bitbucket.io/tag/gdb/">gdb</a>.</p>
        </div>
      </div>
    </footer>
  </article>
</main>
</body>
</html>